import pandas as pd
import matplotlib.pyplot as plt

import pandas as pd
import matplotlib.pyplot as plt

from Histogram import *
from GroupingFile import *


if __name__ == "__main__":

    # uncomment the section you want to run to either group data or plot it

    # # 1. creates a new sheet called Vehicle Trips which removes non-vehicle trips from raw data
    # filename = 'fullSET_3cols.xlsx'
    # sheetname = 'OriginalTripData'
    # getVehicleTrips(filename, sheetname)

    # # 2. creates vehicle trip distibution plot (recreates one from FHTS webpage)
    # file_path = 'fullSET_3cols.xlsx'
    # sheet_name = "VehicleTrips"
    # Column_name = 'TRPMILES'
    # total_count = 26282
    #
    # histogram_specificBins_VT(file_path, sheet_name, Column_name, total_count)

    # # 3. creates a new sheet called TotalVehicleMiles which sorts all the trips that have the same VEH ID and combines them
    # filename = 'fullSET_3cols.xlsx'
    # sheetname = "VehicleTrips"
    # groupTOTmiles(filename, sheetname)

    # # 4. Create histogram without 0 miles included and culiminative percentage
    # file_path = 'fullSET_3cols.xlsx'
    # sheet_name = "TotalVehicleMiles"
    # column_name = 'TRPMILESTOT'
    # histogram_specificBins_TOTMILES_with0miles_without0miles_CulminativePercentage(file_path, sheet_name, column_name)

    # # 5. Create histogram with 0 miles included and culiminative percentage
    # file_path = 'fullSET_3cols.xlsx'
    # sheet_name = "TotalVehicleMiles"
    # column_name = 'TRPMILESTOT'
    # histogram_specificBins_TOTMILES_with0miles_withCulminativePercentage(file_path, sheet_name, column_name)

    # 5. Create histogram that plots total energy to recharge a vehicle based on its efficiency
    vehicleName = '2023 Cadillac LYRIQ'
    vehicleEfficiency = 38 # [kWh/100 miles]
    file_path = 'fullSET_3cols.xlsx'
    sheet_name = "TotalVehicleMiles"
    column_name = 'TRPMILESTOT'
    histogram_specificBins_TOTEnergy_withCulminativePercentage(file_path, sheet_name, column_name, vehicleEfficiency, vehicleName)







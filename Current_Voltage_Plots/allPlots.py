import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from matplotlib.ticker import MultipleLocator, FuncFormatter


def allplots():
    csfont = {'fontname': 'Times New Roman'}

    # Generate x values
    time = 10 * np.pi
    time_values = np.linspace(0, time, 1000)

    # Generate y values using the piecewise function for current
    phase_shift = np.pi / 10  # between current and voltage
    voltagePhaseShift = np.pi / 20  # between voltage and voltage waveforms
    voltage_values = 170 * np.sin(time_values)
    current_values = 10 * np.sin(time_values - phase_shift)
    noise = 0.1 * np.random.normal(0, 0.5, 1000)

    # Create the plot
    fig, (ax1, ax2) = plt.subplots(2, 1)  # Two subplots, one column

    # Function to format x-axis labels as multiples of pi
    def format_func(value, tick_number):
        N = int(np.round(value / np.pi))
        if N == 0:
            return "0"
        elif N == 1:
            return r"$\pi$"
        elif N == -1:
            return r"-$\pi$"
        else:
            return r"${0}\pi$".format(N)

    # Plot real voltage
    ax1.plot(time_values, voltage_values, label='Voltage [V]', color='red')
    ax1.set_ylabel('Voltage [V]', **csfont, color='red', fontsize=18)
    ax1.tick_params(axis='y', labelcolor='red')
    ax1.set_ylim(-180, 180)  # Set y-bounds for the first y-axis
    ax1.text(24, 90, 'True Voltage', horizontalalignment='center', color='red', **csfont, fontsize=16)
    ax1.axvline(2 * np.pi / 4, color='red', linestyle='--', linewidth=1)
    ax1.axvline(2 * np.pi + 2 * np.pi / 4, color='red', linestyle='--', linewidth=1)

    # plot real current
    ax1_current = ax1.twinx()
    ax1_current.plot(time_values, current_values, label='Current [A]', linewidth=3, color='blue')
    ax1_current.set_ylabel('Current [A]', **csfont, color='blue', fontsize=18)
    ax1_current.tick_params(axis='y', labelcolor='blue')
    ax1_current.set_ylim(-12, 12)  # Set y-bounds for the second y-axis
    ax1.text(11, 90, 'True Current', horizontalalignment='center', color='blue', **csfont, fontsize=16)
    ax1.axvline(2 * np.pi / 4 + phase_shift, color='blue', linestyle='--', linewidth=1)

    ax1.text(2.25, 190, '$\Delta = \\theta$', horizontalalignment='center', color='black', **csfont, fontsize=16)

    # Plot voltage logic on the second subplot

    # Plot voltage logic waveform
    ax2.plot(time_values, 2.5 * signal.square(2 * (time_values - voltagePhaseShift + np.pi / 4), 0.5) + 2.5 + noise,
             color='hotpink')

    ax2.set_xlabel('Time', **csfont, fontsize=18)
    ax2.set_ylabel('Voltage logic [V]', **csfont, color='hotpink', fontsize=18)
    ax2.tick_params(axis='y', labelcolor='hotpink')
    ax2.set_ylim(-7, 7)  # Set y-bounds for the third y-axis

    # Plot current logic waveform
    ax2_current = ax2.twinx()
    ax2_current.plot(time_values, 1.2 * np.sin(time_values - phase_shift) + 2.5 + noise, color='cornflowerblue')

    ax2_current.set_ylabel('Current logic [V]', **csfont, color='cornflowerblue', fontsize=18)
    ax2_current.set_ylim(0, 5)  # Set y-bounds for the second y-axis
    # ax2.text(25.1, 15.8, 'Voltage', horizontalalignment='center', color='hotpink', **csfont, fontsize=16)
    # ax2.arrow(27, 16.2, 2, 0, color='hotpink', width=0.2)
    # ax2.text(5, 12.5, 'Current', horizontalalignment='center', color='hotpink', **csfont, fontsize=16)
    # ax2.arrow(3, 13, -2, 0, color='hotpink', width=0.2)
    # ax1.axvline(2 * np.pi + 2*np.pi/4 + phase_shift, color='blue', linestyle='--', linewidth=1)
    # ax2.axvline(2 * np.pi + 2*np.pi/4 + phase_shift, color='blue', linestyle='--', linewidth=1)
    ax2.axvline(2 * np.pi + 2 * np.pi / 4 + voltagePhaseShift, color='hotpink', linestyle='--', linewidth=1)
    ax2.axvline(2 * np.pi + 2 * np.pi / 4, color='red', linestyle='--', linewidth=1)

    ax2_current.axhline(2.5, color='cornflowerblue', linestyle='--', linewidth=1)
    ax2_current.text(np.pi / 2 + phase_shift, 1.8, 'sensor dc-offset', horizontalalignment='center',
                     color='cornflowerblue', **csfont, fontsize=16)
    arrow1 = ax2.arrow(np.pi / 2 + phase_shift, -1, 0, 0.4, color='cornflowerblue', width=0.1)

    ax2.text(8.5, 7.5, '$\Delta = \phi$', horizontalalignment='center', color='black', **csfont, fontsize=16)

    # Set x-axis major ticks to multiples of pi and format them
    ax1.xaxis.set_major_locator(MultipleLocator(base=np.pi))
    ax1.xaxis.set_major_formatter(FuncFormatter(format_func))
    ax2.xaxis.set_major_locator(MultipleLocator(base=np.pi))
    ax2.xaxis.set_major_formatter(FuncFormatter(format_func))

    plt.tight_layout(rect=[0, 0.03, 1, 0.95])  # Adjust the top space for title
    plt.show()

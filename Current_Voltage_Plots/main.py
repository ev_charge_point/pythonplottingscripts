import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from matplotlib.ticker import MultipleLocator, FuncFormatter

from allPlots import*
from currentPlot import*
from voltagePlot import*


allplots() #this prints out all the plots
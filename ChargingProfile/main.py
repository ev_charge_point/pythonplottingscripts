import numpy as np
import matplotlib.pyplot as plt
csfont = {'fontname':'Times New Roman'}


def piecewise_functionCurrent(time):
    if  (0 <= time < 15):
        return 12
    elif (15 < time <= 20):
        return 10
    elif (20 < time < 25):
        return 8
    else:
        return 6

def piecewise_functionVoltage(time):
    if  (0 <= time < 15):
        return 117.8
    elif (15 < time <= 20):
        return 118.10
    elif (20 < time < 25):
        return 118.70
    else:
        return 119.34

# Generate x values
time_values = np.linspace(0, 30, 1000)

# Generate y values using the piecewise function for current
current_values = np.array([piecewise_functionCurrent(time) for time in time_values])

# Generate y values using the piecewise function for voltage
voltage_values = np.array([piecewise_functionVoltage(time) for time in time_values])

# Plot the piecewise functions
fig, (ax1, ax2) = plt.subplots(2, 1)  # Two subplots, one column

# Plot for the first function (current)
ax1.plot(time_values, current_values, label='RMS Current [A]', linewidth=3, color='blue')
#ax1.set_xlabel('Time [minutes]', fontsize = 18)
ax1.set_ylabel('RMS current [A]', **csfont, color='blue', fontsize=18)
ax1.tick_params(axis='y', labelcolor='blue')
ax1.set_ylim(5, 18)  # Set y-bounds for the first y-axis

# Add RMS voltage [V] on the first subplot
ax1_voltage = ax1.twinx()
ax1_voltage.plot(time_values, voltage_values, label='RMS Voltage [V]', color='red')
ax1_voltage.set_ylabel('RMS voltage [V]', **csfont, color='red', fontsize=18)
ax1_voltage.tick_params(axis='y', labelcolor='red')
ax1_voltage.set_ylim(100, 125)  # Set y-bounds for the second y-axis
ax1.text(25.1, 15.8, 'Voltage', horizontalalignment='center', color='red',**csfont, fontsize=16)
ax1.arrow(27, 16.2, 2, 0, color = 'red', width=0.2)
ax1.text(5, 12.5, 'Current', horizontalalignment='center', color='blue',**csfont, fontsize=16)
ax1.arrow(3, 13,-2, 0, color = 'blue', width=0.2)


# Plot for the third function on the second subplot
ax2.plot(time_values, current_values * voltage_values, label='Power [W]', color='green')  # Example: Power = Current * Voltage
ax2.set_xlabel('Time [minutes]', **csfont, fontsize = 18)
ax2.set_ylabel('Power [W]', **csfont, color='green', fontsize=18)
ax2.tick_params(axis='y', labelcolor='green')
ax2.set_ylim(0, 1500)  # Set y-bounds for the third y-axis
ax2.fill_between(time_values, current_values *voltage_values, where=(time_values <= 10), color='yellowgreen', alpha=0.5)  # Shading from 0 to 10
ax2.fill_between(time_values, current_values * voltage_values, where=(time_values>= 10) & (time_values <= 20), color='mediumpurple', alpha=0.4)  # Shading from 10 to 20
ax2.fill_between(time_values, current_values *voltage_values, where=(time_values >= 20) & (time_values <= 30), color='cyan', alpha=0.5)  # Shading from 10 to 20
ax2.text(5, 500, '238 Wh', horizontalalignment='center', **csfont, fontsize=16)
ax2.text(15, 500, '214 Wh', horizontalalignment='center', **csfont, fontsize=16)
ax2.text(25, 500, '141 Wh', horizontalalignment='center', **csfont, fontsize=16)

fig.suptitle('Metering results vs time', y=0.88, **csfont, fontsize=23)
plt.tight_layout(rect=[0, 0.03, 1, 0.95])  # Adjust the top space for title
plt.show()

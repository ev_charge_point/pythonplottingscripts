import openpyxl
import pandas as pd

def groupTOTmiles(filename, sheetname):
    wb = openpyxl.load_workbook(filename)
    ws = wb[sheetname]

    # Copy the sheet
    #wb.copy_worksheet(ws)
    #ws = wb[sheetname + ' Copy']
    wsnew = wb.create_sheet('TotalVehicleMiles')

    i = ws.max_row  # Start from the bottom
    while i >= 2:
        if ws.cell(row=i, column=2).value == -1 or ws.cell(row=i, column=3).value == -9:
            ws.delete_rows(i, 1)  # Delete one row at a time
        i -= 1

    wsnew.cell(row=1, column=1, value='HOUSEID')
    wsnew.cell(row=1, column=2, value='VEHCASEID')
    wsnew.cell(row=1, column=3, value='TRPMILESTOT')

    grouped_data = {}

    for j in range(2, ws.max_row + 1):
        house_id = ws.cell(row=j, column=1).value
        vehicle_id = ws.cell(row=j, column=2).value
        trip_miles = ws.cell(row=j, column=3).value

        key = (house_id, vehicle_id)
        grouped_data[key] = grouped_data.get(key, 0) + trip_miles

    k = 2
    for key, total_trip_miles in grouped_data.items():
        wsnew.cell(row=k, column=1, value=key[0])  # HOUSEID
        wsnew.cell(row=k, column=2, value=key[1])  # VEHCASEID
        wsnew.cell(row=k, column=3, value=total_trip_miles)  # Total TRPMILES
        k += 1

    wb.save(filename)

def group(filename, sheetname, column1name, column2name, condition):

    # Read the Excel file into a pandas DataFrame
    df = pd.read_excel(filename, sheet_name=sheetname)

    # Filter rows where TRPMILES is equal to 0
    filtered_df = df[df[column2name] == condition]

    # Create a new Excel writer object
    with pd.ExcelWriter(filename, engine='openpyxl', mode='a') as writer:
        # Write the filtered DataFrame to a new sheet
        filtered_df.to_excel(writer, sheet_name='Zero_TRPMILES', index=False)

    print("Filtered data has been written to a new sheet 'Zero_TRPMILES'.")


def getVehicleTrips(filename, sheetname):
    #removes non vehicle trips from raw data

    # Read the Excel file into a pandas DataFrame
    df = pd.read_excel(filename, sheet_name=sheetname)

    # Filter rows where VehicleCASEID is greater than 0
    filtered_df = df[(df['VEHCASEID'] > 0) & (df['TRPMILES'] > 0)]

    # Create a new Excel writer object
    with pd.ExcelWriter(filename, engine='openpyxl', mode='a') as writer:
        # Write the filtered DataFrame to a new sheet
        filtered_df.to_excel(writer, sheet_name='VehicleTrips', index=False)

    print("Filtered data has been written to a new sheet 'VehicleTrips'.")


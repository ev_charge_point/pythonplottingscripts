import pandas as pd
import matplotlib.pyplot as plt

import pandas as pd
import matplotlib.pyplot as plt
csfont = {'fontname':'Times New Roman'}

def create_histogram(file_path, sheet_name, ColumnName, title, xaxisName, custom_bins=None):

    # Read the Excel file
    df = pd.read_excel(file_path, sheet_name=sheet_name)

    # Assuming that your data is in a column named 'Values', adjust it accordingly
    values_column = ColumnName

    # Plotting the histogram with custom bin sizes
    bin_counts, bins, _ = plt.hist(df[values_column], bins=custom_bins, alpha=0.7, color='blue', edgecolor='black',
                                   linewidth=1.2)

    # Calculate total count
    total_count = sum(bin_counts)

    # Calculate percentage frequency for each bin
    bin_percentages = [count / total_count * 100 for count in bin_counts]

    # Add text labels above each bar
    for i in range(len(bin_counts)):
        plt.text(bins[i] + 0.5 * (bins[i + 1] - bins[i]), bin_counts[i], f'{bin_percentages[i]:.2f}%', ha='center',
                 va='bottom')

    # Customize the plot
    plt.title(title)
    plt.xlabel(xaxisName)
    plt.ylabel('Frequency')

    # Adjust x-axis ticks for more frequency
    plt.xticks(bins)  # Set the x-ticks to the bin edges

    # Show the plot
    plt.show()


import pandas as pd
import matplotlib.pyplot as plt

import pandas as pd
import matplotlib.pyplot as plt


def histogram_specificBins_VT(file_path, sheet_name, Column_name, total_count):
    # Read the Excel file into a pandas DataFrame
    df = pd.read_excel(file_path, sheet_name=sheet_name)

    # Define the conditions
    condition_1 = df[Column_name] < 0.5
    condition_2 = (df[Column_name] >= 0.5) & (df[Column_name] < 1.5)
    condition_3 = (df[Column_name] >= 1.5) & (df[Column_name] < 2.5)
    condition_4 = (df[Column_name] >= 2.5) & (df[Column_name] < 3.5)
    condition_5 = (df[Column_name] >= 3.5) & (df[Column_name] < 4.5)
    condition_6 = (df[Column_name] >= 4.5) & (df[Column_name] < 5.5)
    condition_7 = (df[Column_name] >= 5.5) & (df[Column_name] < 10.5)
    condition_8 = (df[Column_name] >= 10.5) & (df[Column_name] < 15.5)
    condition_9 = (df[Column_name] >= 15.5) & (df[Column_name] < 20.5)
    condition_10 = (df[Column_name] >= 20.5) & (df[Column_name] < 30.5)
    condition_11 = df[Column_name] >= 30.5

    # Count occurrences for each condition
    count_condition_1 = condition_1.sum()
    count_condition_2 = condition_2.sum()
    count_condition_3 = condition_3.sum()
    count_condition_4 = condition_4.sum()
    count_condition_5 = condition_5.sum()
    count_condition_6 = condition_6.sum()
    count_condition_7 = condition_7.sum()
    count_condition_8 = condition_8.sum()
    count_condition_9 = condition_9.sum()
    count_condition_10 = condition_10.sum()
    count_condition_11 = condition_11.sum()

    # Create a histogram
    conditions = ['', '1', '2', '3', '4', '5', '6 - 10', '11 - 15', '16 - 20', '21 - 30', '>31']
    counts = [count_condition_1, count_condition_2, count_condition_3, count_condition_4, count_condition_5,
              count_condition_6, count_condition_7, count_condition_8, count_condition_9, count_condition_10,
              count_condition_11]

    bar_width = 0.6  # Adjust this value to make bars narrower or wider

    plt.bar(conditions, counts, width=bar_width)
    plt.xlabel('Trip Distance [Miles]', fontsize=40)
    plt.ylabel('Travel Day Vehicle Trips', fontsize=35)
    plt.ylim(0, 5500)

    # Increase the font size of the X-axis ticks and rotate them
    plt.xticks(fontsize=30, rotation=315, ha='center')  # Rotate labels 45 degrees and align them to the right
    plt.yticks(fontsize=15)

    for i, count in enumerate(counts):
        percent = count / total_count * 100
        plt.text(i, count + 0.1, f'{percent:.1f}%', ha='center', va='bottom', fontsize=25)

    plt.title('Number of Vehicle Trips (VT) by Trip Distance (TRPMILES)', fontsize=37)

    # Adjust the bottom margin to make space for the rotated labels
    plt.gcf().subplots_adjust(bottom=0.25)  # Adjust this value as needed

    plt.show()


def histogram_specificBins_TOTMILES_without0miles(file_path, sheet_name, Column_name):
    # Read the Excel file into a pandas DataFrame
    df = pd.read_excel(file_path, sheet_name=sheet_name)

    # Define the conditions
    conditions = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 90, 100, 110, 120, 130, 140]
    bins = [f'{conditions[i]}-{conditions[i + 1]}' for i in range(len(conditions) - 1)]
    bins.append('>140')

    # Create histogram bins
    df['Bins'] = pd.cut(df[Column_name], bins=conditions + [float('inf')], labels=bins, right=False)

    # Count occurrences for each bin
    counts = df['Bins'].value_counts().reindex(bins, fill_value=0)


    # Calculate relative frequencies
    total_count = len(df)
    relative_frequencies = counts / total_count * 100

    # Create the bar plot
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.3)  #
    # Adjust only the size of the plot, not the entire figure
    ax.set_position([0.1, 0.2, 0.8, 0.7])  # [left, bottom, width, height]

    ax.bar(relative_frequencies.index, relative_frequencies,
           color=['blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue',
                  'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue',
                  'blue', 'blue', 'blue', 'blue'])
    # plt.bar(relative_frequencies.index, relative_frequencies, color='blue')
    ax.set_ylim(0, 40)
    ax.set_xlabel('Vehicle Miles', **csfont, fontsize=35)
    ax.set_ylabel('Relative Frequency (%)', **csfont, fontsize=35)
    ax.tick_params(axis='x', rotation=90, labelsize=24)
    # ax.tick_params(axis='x', rotation=90, **csfont, fontsize=9)
    ax.set_title('Total vehicle miles driven on survey day', **csfont, fontsize=35)

    # Add text labels
    for i, count in enumerate(relative_frequencies):
        ax.text(i, count + 0.5, f'{count:.2f}%', rotation=90, ha='center', va='bottom', fontsize=24)

    plt.show()

def histogram_specificBins_TOTMILES_with0miles(file_path, sheet_name, Column_name):
    # Read the Excel file into a pandas DataFrame
    number0datapoints = 3210
    df = pd.read_excel(file_path, sheet_name=sheet_name)

    # Define the conditions
    conditions = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 90, 100, 110, 120, 130, 140]
    bins = [f'{conditions[i]}-{conditions[i+1]}' for i in range(len(conditions) - 1)]
    bins.append('>140')

    # Create histogram bins
    df['Bins'] = pd.cut(df[Column_name], bins=conditions + [float('inf')], labels=bins, right=False)

    # Count occurrences for each bin
    counts = df['Bins'].value_counts().reindex(bins, fill_value=0)

    # Add the count_notravelday bin at the first position
    counts = pd.concat([pd.Series([number0datapoints], index=['0']), counts])

    # Calculate relative frequencies
    total_count = len(df) + number0datapoints
    relative_frequencies = counts / total_count * 100

    # Create the bar plot
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.3)  #
    # Adjust only the size of the plot, not the entire figure
    ax.set_position([0.1, 0.2, 0.8, 0.7])  # [left, bottom, width, height]

    ax.bar(relative_frequencies.index, relative_frequencies, color=['orange', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue',
                                                                           'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue',
                                                                           'blue', 'blue', 'blue', 'blue'])
    # plt.bar(relative_frequencies.index, relative_frequencies, color='blue')
    ax.set_ylim(0, 40)
    ax.set_xlabel('Vehicle Miles', **csfont, fontsize=35)
    ax.set_ylabel('Relative Frequency (%)', **csfont, fontsize=35)
    ax.tick_params(axis='x', rotation=90, labelsize=24)
    #ax.tick_params(axis='x', rotation=90, **csfont, fontsize=9)
    ax.set_title('Total vehicle miles driven on survey day', **csfont, fontsize=35)


    # Add text labels
    for i, count in enumerate(relative_frequencies):
        ax.text(i, count + 0.5, f'{count:.2f}%', rotation=90, ha='center', va='bottom', fontsize=24)

    plt.show()






def histogram_specificBins_TOTMILES_with0miles_without0miles_CulminativePercentage(file_path, sheet_name, column_name):
    try:
        # Read the Excel file into a pandas DataFrame
        df = pd.read_excel(file_path, sheet_name=sheet_name)
    except Exception as e:
        print(f"Error reading Excel file: {e}")
        return

    # Define the conditions
    conditions = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 90, 100, 110, 120, 130, 140]
    bins = [f'{conditions[i]}-{conditions[i+1]}' for i in range(len(conditions) - 1)]
    bins.append('>140')

    # Create histogram bins
    df['Bins'] = pd.cut(df[column_name], bins=conditions + [float('inf')], labels=bins, right=False)

    # Count occurrences for each bin
    counts = df['Bins'].value_counts().reindex(bins, fill_value=0)

    # Add the count_notravelday bin at the first position

    # Calculate relative frequencies
    total_count = len(df)
    relative_frequencies = counts / total_count * 100

    relative_cumulative_frequency = relative_frequencies.cumsum()

    # Create the bar plot
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.3)

    # Adjust only the size of the plot, not the entire figure
    ax.set_position([0.1, 0.2, 0.8, 0.7])  # [left, bottom, width, height]

    colors = ['blue'] * (len(relative_frequencies))

    ax.bar(relative_frequencies.index, relative_frequencies, color=colors)
    ax.set_ylim(0, 40)
    ax.set_xlabel('Vehicle Miles', fontsize=35)
    ax.set_ylabel('Culminative Frequency (%)', fontsize=35)
    ax.tick_params(axis='x', rotation=90, labelsize=24)
    ax.set_title('Total vehicle miles driven on survey day', fontsize=35)

    print(relative_frequencies)
    print(relative_cumulative_frequency)

    # Add text labels
    for i, (freq, cum_freq) in enumerate(zip(relative_frequencies, relative_cumulative_frequency)):
        ax.text(i, freq + 0.5, f'{cum_freq:.2f}%', rotation=90, ha='center', va='bottom', fontsize=24)

    plt.show()
def histogram_specificBins_TOTMILES_with0miles_withCulminativePercentage(file_path, sheet_name, column_name):
    try:
        # Read the Excel file into a pandas DataFrame
        df = pd.read_excel(file_path, sheet_name=sheet_name)
    except Exception as e:
        print(f"Error reading Excel file: {e}")
        return

    number0datapoints = 3210  # Number of days with no travel

    # Define the conditions
    conditions = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 90, 100, 110, 120, 130, 140]
    bins = [f'{conditions[i]}-{conditions[i+1]}' for i in range(len(conditions) - 1)]
    bins.append('>140')

    # Create histogram bins
    df['Bins'] = pd.cut(df[column_name], bins=conditions + [float('inf')], labels=bins, right=False)

    # Count occurrences for each bin
    counts = df['Bins'].value_counts().reindex(bins, fill_value=0)

    # Add the count_notravelday bin at the first position
    counts = pd.concat([pd.Series([number0datapoints], index=['0']), counts])

    # Calculate relative frequencies
    total_count = len(df) + number0datapoints
    relative_frequencies = counts / total_count * 100

    relative_cumulative_frequency = relative_frequencies.cumsum()

    # Create the bar plot
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.35)

    # Adjust only the size of the plot, not the entire figure
    ax.set_position([0.1, 0.2, 0.8, 0.7])  # [left, bottom, width, height]

    colors = ['orange'] + ['blue'] * (len(relative_frequencies) - 1)

    ax.bar(relative_frequencies.index, relative_frequencies, color=colors)
    ax.set_ylim(0, 40)
    ax.set_xlabel('Vehicle Miles', fontsize=35)
    ax.set_ylabel('Culminative Frequency (%)', fontsize=35)
    ax.tick_params(axis='x', rotation=90, labelsize=24)
    ax.set_title('Total vehicle miles driven on survey day', fontsize=35)

    print(relative_frequencies)
    print(relative_cumulative_frequency)

    # Add text labels
    for i, (freq, cum_freq) in enumerate(zip(relative_frequencies, relative_cumulative_frequency)):
        ax.text(i, freq + 0.5, f'{cum_freq:.2f}%', rotation=90, ha='center', va='bottom', fontsize=24)

    plt.show()

import pandas as pd
import matplotlib.pyplot as plt

import pandas as pd
import matplotlib.pyplot as plt

def histogram_specificBins_TOTEnergy_withCulminativePercentage(file_path, sheet_name, column_name, vehicle_efficiency, vehicleName=''):
    try:
        # Read the Excel file into a pandas DataFrame
        df = pd.read_excel(file_path, sheet_name=sheet_name)
    except Exception as e:
        print(f"Error reading Excel file: {e}")
        return

    num_days_no_travel = 3210  # Number of days with no travel

    # Multiply each data point by the fixed value
    df[column_name] = df[column_name] * vehicle_efficiency / 100

    # Define the conditions
    #conditions = [0, 2, 4, 6, 8, 10, 11.5, 14, 17.3, 20, 22, 23.0, 26, 28, 30, 32, 34] #goood for low energy vehicle
    conditions = [0, 2, 4, 6, 8, 10, 11.5, 14, 17.3, 20, 22, 23.0, 26, 28, 30, 32, 34, 36, 38, 40, 42]
    bins = [f'{conditions[i]}-{conditions[i + 1]}' for i in range(len(conditions) - 1)]
    bins.append('>42')

    # Create histogram bins
    df['Bins'] = pd.cut(df[column_name], bins=conditions + [float('inf')], labels=bins, right=False)

    # Count occurrences for each bin
    counts = df['Bins'].value_counts().reindex(bins, fill_value=0)

    # Add the count_notravelday bin at the first position
    counts = pd.concat([pd.Series([num_days_no_travel], index=['0']), counts])

    # Calculate relative frequencies
    total_count = len(df) + num_days_no_travel
    relative_frequencies = counts / total_count * 100

    relative_cumulative_frequency = relative_frequencies.cumsum()

    # Create the bar plot
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.35, right=0.85)  # Adjust right parameter to add space on the right-hand side

    # Adjust only the size of the plot, not the entire figure
    ax.set_position([0.1, 0.2, 0.75, 0.7])  # [left, bottom, width, height]

    colors = ['orange'] + ['blue'] * (len(relative_frequencies) - 1)

    try:
        ax.bar(relative_frequencies.index, relative_frequencies, color=colors)
        ax.set_ylim(0, 40)
        ax.set_xlabel('Daily recharging requirement (kWh)', fontsize=30, labelpad=20)
        ax.set_ylabel('Cumulative Frequency (%)', fontsize=35)
        ax.tick_params(axis='x', rotation=90, labelsize=20)
        ax.set_xlim(-1, len(relative_frequencies)  + 3)
        ax.set_title(vehicleName + ' Daily recharging requirement (kWh) distribution', fontsize=30)

        # Add text labels
        for i, (freq, cum_freq) in enumerate(zip(relative_frequencies, relative_cumulative_frequency)):
            ax.text(i, freq + 0.5, f'{cum_freq:.2f}%', rotation=90, ha='center', va='bottom', fontsize=24)

        plt.show()
    except Exception as e:
        print(f"Error creating plot: {e}")

# Example usage
# histogram_specificBins_TOTEnergy_withCulminativePercentage('path_to_file.xlsx', 'Sheet1', 'ColumnName', 0.95, 'Vehicle Name')
